import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import { createStore, applyMiddleware } from 'redux';
import { BrowserRouter, Route, Switch } from 'react-router-dom';
import promise from 'redux-promise';

import reducers from './reducers';

import Register from './components/auth/register';
import Login from './components/auth/login';

import PollIndex from './components/polls/pollIndex';
import CreatePoll from './components/polls/pollCreate';
import EditPoll from './components/polls/pollEdit';

import QuizIndex from './components/quiz/quizIndex';

import ResultIndex from './components/result/resultIndex';

import TestFrom from './components/playGround/testForm';

const createStoreWithMiddleware = applyMiddleware(promise)(createStore);

ReactDOM.render(
  <Provider store={createStoreWithMiddleware(reducers)}>
    <BrowserRouter>
      <div>
        <Switch>
          <Route path='/register' component={ Register } />
          <Route path='/login' component={ Login } />

          <Route path='/polls' component={ PollIndex } />
          <Route path='/createPoll' component={ CreatePoll } />
          <Route path='/editPoll/:pollId' component={ EditPoll } />

          <Route path='/quizIndex/:pollId' component={ QuizIndex } />
          <Route path='/results/:pollId' component={ ResultIndex } />

          <Route path='/' component={ TestFrom } />

        </Switch>
      </div>
    </BrowserRouter>
  </Provider>
  , document.querySelector('.container'));
