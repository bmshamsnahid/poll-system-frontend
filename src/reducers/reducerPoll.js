import { DELETE_POLL, CREATE_POLL, FETCH_POLLS, FETCH_POLL } from '../actions/poll';
import _ from 'lodash';

export default function(state = {}, action) {
    console.log('Invoking the poll reducer: ');
    switch(action.type) {
        case FETCH_POLLS:
            return _.mapKeys(action.payload.data.polls, 'id');
        case FETCH_POLL:
            const data = action.payload.data;
            return { ...state, [data.id]: data };
        default:
            return state;
    }
}
