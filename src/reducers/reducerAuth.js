import { LOGIN_USER, REGISTER_USER } from '../actions/auth';
import _ from 'lodash';

export default function(state = {}, action) {
    switch(action.type) {
        case LOGIN_USER:
            const authToken = action.payload;
            if (authToken) {
                console.log('Auth Token  is: ');
                console.log(action.payload);
                localStorage.setItem('token', authToken.access_token);
                localStorage.setItem('refreshToken', authToken.refresh_token);
            }
            return state;
        default:
            return state;
    }
}
