import { GET_RESULT } from '../actions/result';
import _ from 'lodash';

export default function(state = {}, action) {
    switch(action.type) {
        case GET_RESULT:
            const data = action.payload.data;
            return { ...state, pollResult: data };
        default:
            return state;
    }
}
