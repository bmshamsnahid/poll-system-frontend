import { CREATE_QUIZ, FETCH_QUIZES, DELETE_QUIZ } from '../actions/quiz';
import _ from 'lodash';

export default function(state = {}, action) {
    switch(action.type) {
        case CREATE_QUIZ:
            return state;
        case DELETE_QUIZ:
            const data = action.payload.data;
            const deletedQuizId = data.id;
            const filteredQuizes = state.quizes.filter(quiz => quiz.id != deletedQuizId);
            return { ...state, quizes: filteredQuizes };
        case FETCH_QUIZES:
            const { quizes } = action.payload.data;
            return { ...state, quizes };

        default:
            return state;
    }
}
