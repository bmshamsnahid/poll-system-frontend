import { CREATE_OPTION, UPDATE_OPTION, DELETE_OPTION } from '../actions/option';
import _ from 'lodash';

export default function(state = {}, action) {
    switch(action.type) {
        case CREATE_OPTION:
            const data = action.payload.data;
            console.log(data);
            return state;
        default:
            return state;
    }
}
