import { combineReducers } from 'redux';
import { reducer as formReducer } from 'redux-form';

import AuthReducer from './reducerAuth';
import PollReducer from './reducerPoll';
import ResultReducer from './reducerResult';
import OptionReducer from './reducerOption';
import QuizReducer from './reducerQuiz';

const rootReducer = combineReducers({
  polls: PollReducer,
  quizes: QuizReducer,
  options: OptionReducer,
  result: ResultReducer,
  auth: AuthReducer,
  form: formReducer,
});

export default rootReducer;
