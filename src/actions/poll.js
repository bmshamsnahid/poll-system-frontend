import axios from 'axios';

export const FETCH_POLLS = 'fetch_polls';
export const FETCH_POLL = 'fetch_poll';
export const CREATE_POLL = 'create_poll';
export const DELETE_POLL = 'delete_poll';

const BASE_URL = 'http://127.0.0.1:5000';

export function fetchPolls() {
    const accessToken = localStorage.getItem('accessToken');
    const headers = {
        'Authorization': accessToken,
    };

    const request = axios.get(`${BASE_URL}/polls`, {
        headers: headers,
    });

    return {
        type: FETCH_POLLS,
        payload: request,
    };
}

export function createPoll(values, callback) {
    const accessToken = localStorage.getItem('accessToken');
    const headers = {
        'Authorization': accessToken,
    };
    const request = axios.post(`${BASE_URL}/poll`, values, { headers: headers })
        .then(() => callback());

    return {
        type: CREATE_POLL,
        payload: request,
    }
}

export function fetchPoll(id) {
    const accessToken = localStorage.getItem('accessToken');
    const headers = {
        'Authorization': accessToken,
    };
    const request = axios.get(`${BASE_URL}/poll?id=${id}`, { headers: headers });

    return {
        type: FETCH_POLL,
        payload: request,
    };
}

export function deletePoll(id, callback) {
    console.log('Deleting the poll');
    const accessToken = localStorage.getItem('accessToken');
    const headers = {
        'Authorization': accessToken,
    };
    const request = axios.delete(`${BASE_URL}/poll?id=${id}`, { headers: headers })
        .then(() => callback());

    return {
        type: DELETE_POLL,
        payload: request,
    };
}
