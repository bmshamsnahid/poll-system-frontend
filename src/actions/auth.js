import axios from 'axios';

export const REGISTER_USER = 'register_user';
export const LOGIN_USER = 'login_user';

const BASE_URL = 'http://127.0.0.1:5000';

export function actionRegisterUser(values, callback) {
    const request = axios.post(`${BASE_URL}/register`, values)
        .then(() => callback());

    return {
        type: REGISTER_USER,
        payload: request,
    }
}

export function actionLoginUser(values, callback) {
    const request = axios.post(`${BASE_URL}/login`, values)
        .then((response) => callback(response));

    return {
        type: LOGIN_USER,
        payload: request,
    }
}
