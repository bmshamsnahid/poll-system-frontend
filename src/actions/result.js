import axios from 'axios';

export const GET_RESULT = 'get_result';

const BASE_URL = 'http://127.0.0.1:5000';

export function getResult(values) {
    const accessToken = localStorage.getItem('accessToken');

    const headers = {
        'Authorization': accessToken,
    };

    const config = {
        headers: headers,
    }

    const request = axios.post(`${BASE_URL}/vote_result`, values, config)

    return {
        type: GET_RESULT,
        payload: request,
    }
}
