import axios from 'axios';

export const CREATE_VOTE = 'create_vote';
export const DELETE_VOTE = 'delete_vote';

const BASE_URL = 'http://127.0.0.1:5000';

export function createVote(values) {
    const accessToken = localStorage.getItem('accessToken');

    const headers = {
        'Authorization': accessToken,
    };

    const config = {
        headers: headers,
    }

    const request = axios.post(`${BASE_URL}/vote`, values, config);

    return {
        type: CREATE_VOTE,
        payload: request,
    }
}

export function deleteVote(values, callback) {
    const accessToken = localStorage.getItem('accessToken');

    const headers = {
            'Authorization': accessToken,
    };

    const request = axios.delete(`${BASE_URL}/vote`, {
        data: values,
        headers: headers,
    }).then(() => callback())

    return {
        type: DELETE_VOTE,
        payload: request,
    }
}
