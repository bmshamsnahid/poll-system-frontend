import axios from 'axios';

export const CREATE_OPTION = 'create_option';
export const UPDATE_OPTION = 'update_option';
export const DELETE_OPTION = 'delete_option';

const BASE_URL = 'http://127.0.0.1:5000';

export function createOption({ name, quiz_id }, callback) {
    const accessToken = localStorage.getItem('accessToken');
    const headers = {
        'Authorization': accessToken,
    };
    const request = axios.post(`${BASE_URL}/option`, {
        name, quiz_id
    }, { headers: headers })
        .then((response) => callback(response));

    return {
        type: CREATE_OPTION,
        payload: request,
    }
}

export function updateOption({id, name, quiz_id}, callback) {
    const accessToken = localStorage.getItem('accessToken');
    const headers = {
        'Authorization': accessToken,
    };
    const request = axios.put(`${BASE_URL}/option?id=${id}`, {
        name, quiz_id
    }, { headers: headers })
        .then(() => callback());

    return {
        type: UPDATE_OPTION,
        payload: request,
    }
}

export function deleteOption(id, callback) {
    const accessToken = localStorage.getItem('accessToken');
    const headers = {
        'Authorization': accessToken,
    };
    const request = axios.delete(`${BASE_URL}/option?id=${id}`, { headers: headers })
        .then(() => callback());

    return {
        type: DELETE_OPTION,
        payload: request,
    }
}
