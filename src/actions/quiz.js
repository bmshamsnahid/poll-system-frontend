import axios from 'axios';

export const FETCH_QUIZES = 'fetch_quizes';
export const FETCH_QUIZ = 'fetch_quiz';
export const CREATE_QUIZ = 'create_quiz';
export const DELETE_QUIZ = 'delete_quiz';
export const UPDATE_QUIZ = 'update_quiz';

const BASE_URL = 'http://127.0.0.1:5000';

export function fetchQuizes(pollId) {
    const accessToken = localStorage.getItem('accessToken');
    const headers = {
        'Authorization': accessToken,
    };

    const request = axios.get(`${BASE_URL}/quizes?id=${pollId}`, {
        headers: headers,
    });

    return {
        type: FETCH_QUIZES,
        payload: request,
    };
}

export function createQuiz(values, callback) {
    const accessToken = localStorage.getItem('accessToken');
    const headers = {
        'Authorization': accessToken,
    };
    const request = axios.post(`${BASE_URL}/quiz`, values, { headers: headers })
    .then(() => callback());

    return {
        type: CREATE_QUIZ,
        payload: request,
    }
}

export function updateQuiz({id, name, type, poll_id}, callback) {
    const accessToken = localStorage.getItem('accessToken');
    const headers = {
        'Authorization': accessToken,
    };
    const request = axios.put(`${BASE_URL}/quiz?id=${id}`, {
        name, type, poll_id
    }, { headers: headers })
        .then(() => callback());

    return {
        type: UPDATE_QUIZ,
        payload: request,
    }
}

export function deleteQuiz(quizId) {
    const accessToken = localStorage.getItem('accessToken');
    const headers = {
        'Authorization': accessToken,
    };
    const request = axios.delete(`${BASE_URL}/quiz?id=${quizId}`, {
        headers: headers
    });

    return {
        type: DELETE_QUIZ,
        payload: request,
    }
}
