import React, { Component } from 'react';
import { Field, reduxForm } from 'redux-form';
import { connect } from 'react-redux';
import { actionRegisterUser } from '../../actions/auth';
import { Link } from 'react-router-dom';

class Register extends Component {
    renderField(field) {
        const { meta: { touched, error } } = field;
        const className = `form-group ${touched && error ? 'has-danger' : '' }`;
        return (
            <div className={className}>
                <label>{field.label}</label>
                <input
                    type={field.type}
                    className='form-control'
                    {...field.input}
                />
                <div className='text-help'>
                    {touched ? error : ''}
                </div>
            </div>
        )
    }

    onSubmit(values) {
        this.props.actionRegisterUser(values, () => {
            this.props.history.push('/login');
        });
    }

    render() {
        const { handleSubmit } = this.props;

        return (
            <form onSubmit={handleSubmit(this.onSubmit.bind(this))}>
                <Field
                    type='text'
                    label='Username'
                    name='username'
                    component={this.renderField}
                />
                <Field
                    type='text'
                    label='Password'
                    name='password'
                    component={this.renderField}
                />
                <Field
                    type='text'
                    label='Email'
                    name='email'
                    component={this.renderField}
                />
                <Field name="role" component="select">
                    <option value='header' >Select a role</option>
                    <option value="user">User</option>
                    <option value="admin">Admin</option>
                    <option value="super_admin">Super Admin</option>
                </Field>
                <br />
                <button type='submit' className='btn btn-primary'>Submit</button>
                <Link to='/' className='btn btn-danger'>Cancel</Link>
            </form>
        );
    }
}

function validate(values) {
    const errors = {};

    // validate the inputs from values
    if (!values.username) {
        errors.username = 'Enter the username.';
    }

    if (!values.password) {
        errors.password = 'Enter the password.';
    }

    if (!values.email) {
        errors.email = 'Enter the email.';
    }

    if (!values.role) {
        errors.role = 'Enter the role.';
    }

    return errors;
}

export default reduxForm({
    validate,
    form: 'PostRegisterForm',
})(
    connect(null, { actionRegisterUser })(Register)
);
