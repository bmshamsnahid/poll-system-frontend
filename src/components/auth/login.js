import React, { Component } from 'react';
import { Field, reduxForm } from 'redux-form';
import { connect } from 'react-redux';
import { actionLoginUser } from '../../actions/auth';
import { Link } from 'react-router-dom';

class Register extends Component {
    renderField(field) {
        const { meta: { touched, error } } = field;
        const className = `form-group ${touched && error ? 'has-danger' : '' }`;
        return (
            <div className={className}>
                <label>{field.label}</label>
                <input
                    type={field.type}
                    className='form-control'
                    {...field.input}
                />
                <div className='text-help'>
                    {touched ? error : ''}
                </div>
            </div>
        )
    }

    onSubmit(values) {
        this.props.actionLoginUser(values, ({ data: { access_token, refresh_token, user_id } }) => {
            localStorage.setItem('accessToken', `Bearer ${access_token}`);
            localStorage.setItem('refreshToken', refresh_token);
            localStorage.setItem('userId', user_id)
            this.props.history.push('/polls');
        });
    }

    render() {
        const { handleSubmit } = this.props;

        return (
            <form onSubmit={handleSubmit(this.onSubmit.bind(this))}>
                <Field
                    type='text'
                    label='Username'
                    name='username'
                    component={this.renderField}
                />
                <Field
                    type='text'
                    label='Password'
                    name='password'
                    component={this.renderField}
                />
                <br />
                <button type='submit' className='btn btn-primary'>Submit</button>
                <Link to='/' className='btn btn-danger'>Cancel</Link>
            </form>
        );
    }
}

function validate(values) {
    const errors = {};

    // validate the inputs from values
    if (!values.username) {
        errors.username = 'Enter the username.';
    }

    if (!values.password) {
        errors.password = 'Enter the password.';
    }

    if (!values.email) {
        errors.email = 'Enter the email.';
    }

    if (!values.role) {
        errors.role = 'Enter the role.';
    }

    return errors;
}

function mapStateToProps(state, ownProps) {
    console.log('State is: ')
    console.log(state)
    return {
        state
    }
}

export default reduxForm({
    validate,
    form: 'PostLoginForm',
})(
    connect(null, { actionLoginUser })(Register)
);
