import React, { Component } from 'react';
import { Field, reduxForm } from 'redux-form';
import { connect } from 'react-redux';
import { createPoll } from '../../actions/poll';
import { Link } from 'react-router-dom';

class CreatePoll extends Component {
    renderField(field) {
        const { meta: { touched, error } } = field;
        const className = `form-group ${touched && error ? 'has-danger' : '' }`;
        return (
            <div className={className}>
                <label>{field.label}</label>
                <input
                    type='text'
                    className='form-control'
                    {...field.input}
                />
                <div className='text-help'>
                    {touched ? error : ''}
                </div>
            </div>
        )
    }

    onSubmit(values) {
        this.props.createPoll(values, () => {
            this.props.history.push('/polls');
        });
    }

    render() {
        const { handleSubmit } = this.props;

        return (
            <form onSubmit={handleSubmit(this.onSubmit.bind(this))}>
                <Field
                    label='Title'
                    name='title'
                    component={this.renderField}
                />

                <button type='submit' className='btn btn-primary'>Submit</button>
                <Link to='/' className='btn btn-danger'>Cancel</Link>
            </form>
        );
    }
}

function validate(values) {
    const errors = {};

    // validate the inputs from values
    if (!values.title) {
        errors.title = 'Enter the title.';
    }

    return errors;
}

export default reduxForm({
    validate,
    form: 'CreatePollForm',
})(
    connect(null, { createPoll })(CreatePoll)
);
