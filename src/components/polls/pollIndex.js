import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';

import { fetchPolls, deletePoll } from '../../actions/poll';

class PollIndex extends Component {
    componentDidMount() {
        this.props.fetchPolls();
    }

    onDeletePoll(event) {
        const pollId = event.currentTarget.attributes['poll_id'].value;
        console.log('Clicked to delete the poll id: ' + pollId);
        this.props.deletePoll(pollId, () => {
            this.props.fetchPolls();
        });
    }

    renderPolls() {
        return _.map(this.props.polls, poll => {
            return (
                <li className='list-group-item' key={poll.id}>
                    <Link to={`/quizIndex/${poll.id}`}>{poll.title}</Link>
                    <Link className='text-xs-right' to={`/editPoll/${poll.id}`}>
                        ---- Edit ----
                    </Link>
                    <button
                        poll_id={ poll.id }
                        onClick={ this.onDeletePoll.bind(this) }
                    >Delete
                    </button>
                </li>
            )
        })
    }

    render() {
        return (
            <div>
                <div className='text-xs-right'>
                    <Link className='btn btn-primary' to='/createPoll'>
                        Add a poll
                    </Link>
                </div>
                <h3>
                    Polls
                </h3>
                <ul className='list-group'>
                    { this.renderPolls() }
                </ul>
            </div>
        )
    }
}

function mapStateToProps(state) {
    return { polls: state.polls }
}

export default connect(mapStateToProps, {fetchPolls, deletePoll})(PollIndex);
