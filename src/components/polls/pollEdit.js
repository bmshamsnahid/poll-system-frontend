import React, { Component } from 'react';
import { connect } from 'react-redux';

import { deleteQuiz, fetchQuizes, createQuiz } from '../../actions/quiz';
import EditQuiz from '../quiz/editQuiz';
import AddQuiz from '../quiz/addQuiz';


class EditPoll extends Component {
    constructor(props) {
        super(props);
        this.testVal = 'test value';
        this.updateQuizList = this.updateQuizList.bind(this);
    }

    componentDidMount() {
        const { pollId } = this.props.match.params;
        this.props.fetchQuizes(pollId);
    }

    onClickDeleteQuiz(event) {
        const quizId = event.currentTarget.attributes['quiz_id'].value;
        this.props.deleteQuiz(quizId);
    }

    updateQuizList() {
        const { pollId } = this.props.match.params;
        this.props.fetchQuizes(pollId);
        // console.log('Test Value: ' + this.testVal);
        // console.log('Require to update the quiz list.');
    }

    renderQuizes() {
        return this.props.quizes.map(quiz => {
            return (
                <div>
                    <div className='text-xs-right'>
                        <button
                            quiz_id={ quiz.id }
                            onClick={ this.onClickDeleteQuiz.bind(this) }
                        >Delete Quiz
                        </button>
                    </div>
                    <EditQuiz quiz={quiz} pollId={this.props.match.params.pollId} />
                </div>
            )
        });
    }

    render() {
        if (this.props.quizes) {
            return (
                <div>
                    <AddQuiz updateQuizList={ this.updateQuizList } pollId={ this.props.match.params.pollId } />
                    <hr />
                    { this.renderQuizes() }
                </div>
            )
        } else {
            return (
                <div>
                    Loading ...
                </div>
            )
        }
    }
}

function mapStateToProps(state, ownProps) {
    const pollId = ownProps.match.params.pollId;

    return {
        poll: state.polls[pollId],
        quizes: state.quizes.quizes,
    }
}

export default connect(mapStateToProps, { fetchQuizes, deleteQuiz, createQuiz })(EditPoll);
