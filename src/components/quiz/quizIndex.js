import React, { Component } from 'react';
import { connect } from 'react-redux';

import { fetchPoll } from '../../actions/poll';
import { createVote, deleteVote } from '../../actions/vote';

class QuizIndex extends Component {
    constructor(props) {
        super(props);
        this.handleSubmit = this.handleSubmit.bind(this);

        this.pollId = this.props.match.params.pollId;
        this.userId = localStorage.getItem('userId');
        this.votes = [];

        this.CONSTENT_VALUE = 'Default Value';
    }

    componentDidMount() {
        const { pollId } = this.props.match.params;
        this.props.fetchPoll(pollId);
    }

    // For dropDown and radio button
    // if a vote exist with same userId and
    // quizId, update with new vote value
    // else, insert the new vote
    updateVote(vote) {
        let isFound = false;
        this.votes.map(vt => {
            if (
                vt.user_id == vote.user_id
                && vt.poll_id == vote.poll_id
                && vt.quiz_id == vote.quiz_id
            ) {
                vt.option_id = vote.option_id;
                isFound = true;
                return;
            }
        });
        if (!isFound) {
            this.votes.push(vote);
        }
        return;
    }

    // For check boxes
    // if flag is true, insert the vote
    // if flag is false, remove the vote
    modarateCheckBoxValue(vote, flag) {
        if (flag) {
            this.votes.push(vote);
            return;
        }
        const newVotes = [];
        this.votes.map(vt => {
            if (!(vt.user_id == vote.user_id && vt.poll_id == vote.poll_id
                && vt.quiz_id == vote.quiz_id && vt.option_id == vote.option_id)) {
                newVotes.push(vt);
            }
        });
        this.votes = newVotes;
        return;
    }

    onRadioButtonChange(event) {
        const quizId = event.currentTarget.attributes['quiz_id'].value;
        const optionId = event.target.value;
        const vote = {
            user_id: this.userId,
            poll_id: this.pollId,
            quiz_id: quizId,
            option_id: optionId,
        };
        this.updateVote(vote);
    }

    onDropDownChange (event) {
        const quizId = event.currentTarget.attributes['quiz_id'].value;
        const optionId = event.target.value;
        if (optionId != this.CONSTENT_VALUE) {
            const vote = {
                user_id: this.userId,
                poll_id: this.pollId,
                quiz_id: quizId,
                option_id: optionId,
            };
            this.updateVote(vote);
        }
    }

    onCheckBoxChange(event) {
        const quizId = event.currentTarget.attributes['quiz_id'].value;
        const optionId = event.target.value;
        const checkedFlag = event.target.checked;
        const vote = {
            user_id: this.userId,
            poll_id: this.pollId,
            quiz_id: quizId,
            option_id: optionId,
        };
        this.modarateCheckBoxValue(vote, checkedFlag);
    }

    renderDropDownQuizOption(options, quizId) {
        return options.map(option => {
            return (
                <option
                    value={ option.id }
                    quiz_id={ quizId }
                >{ option.name }
                </option>
            )
        });
    }

    renderCheckboxQuizOption(options, quizId) {
        return options.map(option => {
            return (
                <div>
                    <input
                        onClick={ this.onCheckBoxChange.bind(this) }
                        type="checkbox"
                        value={ option.id }
                        quiz_id={ quizId }
                    /> { option.name }
                </div>
            )
        });
    }

    renderRadioQuizOption(options, quizId) {
        return options.map(option => {
            return (
                <label>
                    <input
                        name={ option.quiz_id }
                        type="radio"
                        value={ option.id }
                        quiz_id={ quizId }
                        onChange={ this.onRadioButtonChange.bind(this) }
                    />
                    { option.name }
                </label>
            )
        });
    }

    renderQuizes(quizes) {
        return quizes.map(quiz => {
            if (quiz.type == 'drop_down_list') {
                return (
                    <div>
                        <h3>{quiz.name}</h3>
                        <select
                            quiz_id={quiz.id}
                            onChange={ this.onDropDownChange.bind(this) }>
                            <option
                                value={ this.CONSTENT_VALUE }
                            >Select An Option
                            </option>
                            { this.renderDropDownQuizOption(quiz.options, quiz.id) }
                        </select>
                    </div>
                )
            } else if (quiz.type == 'radio') {
                return (
                    <div>
                        <h3>{quiz.name}</h3>
                        { this.renderRadioQuizOption(quiz.options, quiz.id) }
                    </div>
                )
            } else if (quiz.type == 'check_box') {
                return (
                    <div>
                        <h3>{quiz.name}</h3>
                        { this.renderCheckboxQuizOption(quiz.options, quiz.id) }
                    </div>
                )
            } else {
                return (
                    <div>
                        Invalid quiz type
                    </div>
                )
            }
        });
    }


    handleSubmit(e) {
        e.preventDefault();
        let votes = this.votes;

        this.props.deleteVote({
            'user_id': this.userId,
            'poll_id': this.pollId,
        }, () => {
            for(let index=0; index<votes.length; index++) {
                const vote = votes[index];
                this.props.createVote(vote, this.pollId, this.userId);
            }
        }).then(() => {
            this.props.history.push(`/results/${this.pollId}`);
        });
    }

    render() {
        if (this.props.poll) {
            const pollInfo = this.props.poll;
            return (
                <form onSubmit={this.handleSubmit}>
                    { this.renderQuizes(pollInfo.quizes) }
                    <br />
                    <input type="submit" value="Submit" />
                </form>
            )
        } else {
            return (
                <div>
                    Loading...
                </div>
            )
        }
    }
}

function mapStateToProps(state, ownProps) {
    const pollId = ownProps.match.params.pollId;
    return { poll: state.polls[pollId] }
}

export default connect(mapStateToProps, {fetchPoll, createVote, deleteVote})(QuizIndex);
