import React, { Component } from 'react';
import { connect } from 'react-redux';

import { createQuiz } from '../../actions/quiz';

class AddQuiz extends Component {

    constructor(props) {
        super(props);
        this.CONSTENT_VALUE = 'Select a quiz type';
    }

    componentDidMount() {
        this.setState({
            name: '',
            type: '',
            poll_id: this.props.pollId,
        });
    }

    onButtonClicked(event) {
        if ((this.state.type.length > 0) && (this.state.name.length > 0)) {
            this.props.createQuiz(this.state, () => {
                this.props.updateQuizList();
            });
        }
    }

    onDropDownChange(event) {
        this.setState({
            type: event.target.value
        })
    }

    onNameInputChange(event) {
        this.setState({
            name: event.target.value,
        });
    }

    render() {
        return (
            <div>
                <label>Name: </label>
                <input
                    type='text'
                    value={ this.state ? this.state.name : '' }
                    onChange={ this.onNameInputChange.bind(this) }
                />
                <br />
                <label>Type: </label>
                <select
                    onChange={ this.onDropDownChange.bind(this) }>
                    <option
                        value={ this.CONSTENT_VALUE }
                    >Select An Option
                    </option>
                    <option
                        value='drop_down_list'
                    >Drop Down
                    </option>
                    <option
                        value='check_box'
                    >Check Box
                    </option>
                    <option
                        value='radio'
                    >Radio Button
                    </option>
                </select>
                <br />
                <button onClick={this.onButtonClicked.bind(this)}>Add Quiz</button>
            </div>
        );
    }
}

function mapStateToProps(state, ownProps) {
    return {}
}

export default connect(mapStateToProps, { createQuiz })(AddQuiz);
