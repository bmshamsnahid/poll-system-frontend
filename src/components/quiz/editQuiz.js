import React, { Component } from 'react';

import { updateQuiz } from '../../actions/quiz';
import { updateOption, deleteOption, createOption } from '../../actions/option';

class EditQuiz extends Component {
    constructor(props) {
        super(props);
        this.state = {}
    }

    componentDidMount() {
        this.setState(this.props.quiz);
    }

    componentWillReceiveProps(props) {
        this.setState(props.quiz);
    }

    isNewOption(optionId) {
        let option = {};
        const options = this.state.options;
        options.map(opt => {
            if (opt.id == optionId) {
                option = opt;
                return;
            }
        });
        if (typeof option.newOption != 'undefined' && option.newOption == true) {
            // option.newOption = false;
            return true;
        }
        return false;
    }

    detectQuizType(quizType) {
        switch (quizType) {
            case 'drop_down_list':
                return 'Dropdown';
            case 'check_box':
                return 'Checkbox';
            case 'radio':
                return 'Radio';
            default:
                return 'Invalid Type';
        }
    }

    updateOptionName(optionId, newId, newName) {
        let updatedQuiz = this.state;
        updatedQuiz.options.map(option => {
            if (option.id == optionId) {
                option.id = newId;
                option.name = newName;
                option.newOption = false;
                return;
            }
        });
        this.setState(updatedQuiz);
    }

    deleteOption(optionId) {
        let options = this.state.options.filter(option => {
            if (option.id != optionId) {
                return option;
            }
        });
        this.state.options = options;
        let newState = this.state;
        this.setState(newState);
    }

    addOption() {
        const newOption = {
            id: this.state.options.length + 1,
            name: '',
            quizId: this.state.id,
            newOption: true,
        };
        let options = this.state.options;
        options.push(newOption);
        this.state.options = options;
        let newState = this.state;
        this.setState(newState);
    }

    onTitleInputChange(event) {
        const newQuizName = event.target.value;
        const updatedQuiz = {
            id: this.state.id,
            name: newQuizName,
            type: this.state.type,
            poll_id: this.state.poll_id,
        }
        updateQuiz(updatedQuiz, () => {
            this.setState({
                name: newQuizName,
            });
        });
    }

    onOptionInputChange(event) {
        const newOptionName = event.target.value;
        const optionId = event.currentTarget.attributes['option_id'].value;

        const updatedOption = {
            id: optionId,
            name: newOptionName,
            quiz_id: this.state.id,
        }

        if (this.isNewOption(optionId)) {
            createOption(updatedOption, (response) => {
                const newId = response.data.id;
                const newName = response.data.name;
                this.updateOptionName(optionId, newId, newName);
            });
        } else {
            updateOption(updatedOption, () => {
                this.updateOptionName(optionId, optionId, newOptionName);
            });
        }
    }

    onDeleteOption(event) {
        const optionId = event.currentTarget.attributes['option_id'].value;
        deleteOption(optionId, () => {
            this.deleteOption(optionId);
        });
    }

    onCreateOptionBox(event) {
        return this.addOption();
    }

    renderOptions() {
        if (this.state.options) {
            return this.state.options.map(option => {
                return (
                    <div>
                        <input
                            type='text'
                            name='quizName'
                            value={ option.name }
                            option_id={ option.id }
                            onChange={ this.onOptionInputChange.bind(this) }
                        />
                        <button
                            option_id={ option.id }
                            onClick={ this.onDeleteOption.bind(this) }
                        >  -
                        </button>
                    </div>
                );
            });
        }
    }

    renderQuiz() {
        return (
            <div>
                <h2>Quiz Name:  { this.state.name }</h2>
                <h3>Field Type:  { this.detectQuizType(this.state.type) }</h3>
                <label>
                    <h4>Title:</h4>
                    <input
                        type='text'
                        name='quizName'
                        value={ this.state.name }
                        onChange={ this.onTitleInputChange.bind(this) }
                    />
                </label>
                <h4>Options</h4>
                { this.renderOptions() }
                <button onClick={ this.onCreateOptionBox.bind(this) }>+</button>
                <hr />
            </div>
        )
    }

    render() {
        if (this.props.quiz) {
            return (
                <div>
                    { this.renderQuiz() }
                </div>
            );
        } else {
            return (
                <div>
                    Loading ...
                </div>
            );
        }
    }
}

export default EditQuiz;
