import React, { Component } from 'react';
import { connect } from 'react-redux';

import { fetchPoll } from '../../actions/poll';
import { getResult } from '../../actions/result';

class ResultIndex extends Component {
    componentDidMount() {
        const { pollId } = this.props.match.params;
        this.props.fetchPoll(pollId);
        this.props.getResult({ 'poll_id': pollId });

    }

    findTotalVotesOfQuiz(pollId, quizId) {
        const votes = this.props.result.votes;
        let totalVotes = 0;
        votes.map(vote => {
            if ((vote.poll_id == pollId) && (vote.quiz_id == quizId) && (vote.option_id != null)) {
                totalVotes ++;
            }
        });
        return totalVotes;
    }

    findTotalVoteOfOption(pollId, quizId, optionId) {
        const votes = this.props.result.votes;
        let totalVotes = 0;
        votes.map(vote => {
            if ((vote.poll_id == pollId)
                    && (vote.quiz_id == quizId)
                    && (vote.option_id == optionId)) {
                totalVotes ++;
            }
        });
        return totalVotes;
    }

    updatePollInformation(poll) {
        poll.quizes.map(quiz => {
            quiz.totalVotes = this.findTotalVotesOfQuiz(poll.id, quiz.id);
            quiz.options.map(option => {
                option.totalVotes = quiz.totalVotes + ' / ' + this.findTotalVoteOfOption(poll.id, quiz.id, option.id);
            });
        });
        return poll;
    }

    printPollHelper(poll) {
        for (let index=0; index<poll.quizes.length; index++) {
            const quiz = poll.quizes[index];
            for (let index2=0; index2<quiz.options.length; index2++) {
                const option = quiz.options[index2];
            }
        }
    }

    renderOption(option) {
        return(
            <div>
                { option.name } - { option.totalVotes }
            </div>
        )
    }

    renderQuiz(quiz) {
        return(
            <div>
                <h3>Quiz Name: { quiz.name }</h3>
                { quiz.options.map(option => this.renderOption(option)) }
            </div>
        )
    }

    renderPoll(poll) {
        return(
            <div>
                <h1>
                    Poll Title: { poll.title }
                </h1>
                { poll.quizes.map(quiz => this.renderQuiz(quiz)) }
            </div>
        )
    }

    render() {
        if (this.props.poll && this.props.result) {
            const poll = this.updatePollInformation(this.props.poll);
            // this.printPollHelper(poll)

            return (
                <div>
                    { this.renderPoll(poll) }
                </div>
            )
        } else {
            return (
                <div>
                    Loading...
                </div>
            )
        }
    }
}

function mapStateToProps(state, ownProps) {
    const pollId = ownProps.match.params.pollId;
    return {
        poll: state.polls[pollId],
        result: state.result.pollResult,
    }
}

export default connect(mapStateToProps, {fetchPoll, getResult})(ResultIndex)
